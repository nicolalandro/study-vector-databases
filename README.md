# Study Vector database
In this repo I put some example of usage of vector databases in particular to find an alternative to elasticsearch

## Elasticsearch
The first test is about elasticsearch with examples of usage:

```
cd elasticsearch
docker-compose up
# elastic localhost:9200
# jupyter localhost:8888
# grafana loaclhost:3000
```

## Qdrant
The first test is about elasticsearch with examples of usage:

```
cd elasticsearch
docker-compose up
# qdrant localhost:6333
# qdrant localhost:6334
# jupyter localhost:8888
```

## Opensearch (WIP)
The first test is about elasticsearch with examples of usage:

```
cd elasticsearch
docker-compose up
# opensearch localhost:9200
# jupyter localhost:8888
# grafana loaclhost:3000
```